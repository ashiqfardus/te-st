<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'uses' => 'IndexController@index',
    'as'=>'index'
]);

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');

Route::get('/index',[
    'uses' => 'IndexController@index',
    'as'=>'index'
]);

Route::get('/about',[
    'uses' => 'AboutController@about',
    'as'=>'about'
]);
Route::get('contact',[
    'uses'=>'IndexController@contact',
    'as'=>'contact'
]);

Route::get('student_apply',[
    'uses'=>'IndexController@student_apply',
    'as'=>'student_apply'
]);

Route::post('student_form',[
    'uses'=>'IndexController@student_form',
    'as'=>'student_form'
]);

Route::get('student/fetch',[
    'uses'=>'IndexController@student_fetch',
    'as'=>'student.fetch'
]);

Route::get('course_details/fetch',[
    'uses'=>'IndexController@student_fetch',
    'as'=>'student.fetch'
]);


Route::get('course/{id}',[
    'uses'=>'IndexController@course_page',
    'as'=>'course'
]);
Route::get('course_details/{id}',[
    'uses'=>'IndexController@course_details',
    'as'=>'course_details'
]);

Route::get('teacher_apply',[
    'uses'=>'IndexController@teacher_apply',
    'as'=>'teacher_apply'
]);

Route::post('teacher_form',[
    'uses'=>'IndexController@teacher_form',
    'as'=>'teacher_form'
]);

Route::post('contact/mail',[
    'uses'=>'IndexController@mail',
    'as'=>'contact.mail'
]);

//Admin Routes

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function() {

    //Slider Routes

    Route::get('/slider/create',[
        'uses'=>'SliderController@create',
        'as'=>'slider.create'
    ]);

    Route::post('/slider/store',[
        'uses'=>'SliderController@store',
        'as'=>'slider.store'
    ]);

    Route::get('slider/show',[
       'uses'=>'SliderController@index',
       'as'=>'slider.index'
    ]);
    Route::get('slider/edit/{id}',[
        'uses'=>'SliderController@edit',
        'as'=>'slider.edit'
    ]);
    Route::post('slider/update/{id}',[
        'uses'=>'SliderController@update',
        'as'=>'slider.update'
    ]);

    Route::get('slider/delete/{id}',[
        'uses'=>'SliderController@destroy',
        'as'=>'slider.delete'
    ]);

    //Logo Routes
    ROute::get('logo/index',[
       'uses'=>'LogoController@index',
       'as'=>'logo.index'
    ]);

    Route::get('logo/edit/{id}',[
        'uses'=>'LogoController@edit',
        'as'=>'logo.edit'
    ]);

    Route::post('logo/update/{id}',[
        'uses'=>'LogoController@update',
        'as'=>'logo.update'
    ]);

    //Contact Routes
    Route::get('contact/index',[
        'uses'=>'ContactController@index',
        'as'=>'contact.index'
    ]);

    Route::get('contact/edit/{id}',[
        'uses'=>'ContactController@edit',
        'as'=>'contact.edit'
    ]);

    Route::post('contact/update/{id}',[
        'uses'=>'ContactController@update',
        'as'=>'contact.update'
    ]);


    //Social Link routes
    Route::get('social/index',[
       'uses'=>'SocialController@index',
       'as'=>'social.index'
    ]);
    Route::get('social/edit/{id}',[
        'uses'=>'SocialController@edit',
        'as'=>'social.edit'
    ]);

    Route::post('social/update/{id}',[
        'uses'=>'SocialController@update',
        'as'=>'social.update'
    ]);


    //Color Routes
    Route::get('color/index',[
       'uses'=>'ColorController@index',
       'as'=>'color.index'
    ]);

    Route::post('color/primary/{id}',[
        'uses'=>'ColorController@primary',
        'as'=>'color.primary'
    ]);

    Route::post('color/secondary/{id}',[
        'uses'=>'ColorController@secondary',
        'as'=>'color.secondary'
    ]);

    Route::post('color/header/{id}',[
        'uses'=>'ColorController@header',
        'as'=>'color.header'
    ]);

    //About Us Routes

    Route::get('about/index',[
        'uses'=>'AboutController@index',
        'as'=>'about.index'
    ]);

    Route::get('about/edit/{id}',[
        'uses'=>'AboutController@edit',
        'as'=>'about.edit'
    ]);

    Route::post('about/update/{id}',[
        'uses'=>'AboutController@update',
        'as'=>'about.update'
    ]);

    //Course Routes
    Route::get('course/index',[
        'uses'=>'CourseController@index',
        'as'=>'course.index'
    ]);

    Route::get('course/edit/{id}',[
        'uses'=>'CourseController@edit',
        'as'=>'course.edit'
    ]);

    Route::post('course/update/{id}',[
        'uses'=>'CourseController@update',
        'as'=>'course.update'
    ]);

    Route::get('course/delete/{id}',[
        'uses'=>'CourseController@destroy',
        'as'=>'course.delete'
    ]);

    Route::get('/course/create',[
        'uses'=>'CourseController@create',
        'as'=>'course.create'
    ]);

    Route::post('/course/store',[
        'uses'=>'CourseController@store',
        'as'=>'course.store'
    ]);

    //Sub-course routes
    Route::get('/sub-course/index',[
        'uses'=>'SubCourseController@index',
        'as'=>'sub-course.index'
    ]);
    Route::get('/sub-course/create',[
        'uses'=>'SubCourseController@create',
        'as'=>'sub-course.create'
    ]);
    Route::POST('/sub-course/store',[
        'uses'=>'SubCourseController@store',
        'as'=>'sub-course.store'
    ]);
    Route::get('/sub-course/edit/{id}',[
        'uses'=>'SubCourseController@edit',
        'as'=>'sub-course.edit'
    ]);

    Route::get('/sub-course/popular/{id}',[
        'uses'=>'SubCourseController@popular',
        'as'=>'sub-course.popular'
    ]);

     Route::get('/sub-course/active_status/{id}',[
        'uses'=>'SubCourseController@active_status',
        'as'=>'sub-course.active_status'
    ]);
    Route::get('/sub-course/ongoing/{id}',[
        'uses'=>'SubCourseController@ongoing',
        'as'=>'sub-course.ongoing'
    ]);


    Route::get('/sub-course/delete/{id}',[
        'uses'=>'SubCourseController@destroy',
        'as'=>'sub-course.delete'
    ]);

    Route::post('sub-course/update/{id}',[
        'uses'=>'SubCourseController@update',
        'as'=>'sub-course.update'
    ]);



    //Testimonial routes
    Route::get('/testimonial/index',[
        'uses'=>'TestimonialController@index',
        'as'=>'testimonial.index'
    ]);
    Route::get('/testimonial/create',[
        'uses'=>'TestimonialController@create',
        'as'=>'testimonial.create'
    ]);
    Route::POST('/testimonial/store',[
        'uses'=>'TestimonialController@store',
        'as'=>'testimonial.store'
    ]);
    Route::get('/testimonial/edit/{id}',[
        'uses'=>'TestimonialController@edit',
        'as'=>'testimonial.edit'
    ]);

    Route::get('/testimonial/delete/{id}',[
        'uses'=>'TestimonialController@destroy',
        'as'=>'testimonial.delete'
    ]);

    Route::post('testimonial/update/{id}',[
        'uses'=>'TestimonialController@update',
        'as'=>'testimonial.update'
    ]);

    Route::get('student/index',[
       'uses'=>'StudentController@index',
       'as'=>'student.index'
    ]);

    Route::get('student/delete/{id}',[
       'uses'=>'StudentController@delete',
       'as'=>'student.delete'
    ]);

    Route::get('instructor/index',[
        'uses'=>'InstructorController@index',
        'as'=>'instructor.index'
    ]);

    Route::get('instructor/delete/{id}',[
        'uses'=>'InstructorController@delete',
        'as'=>'instructor.delete'
    ]);
});
