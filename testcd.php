<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="http://irtbd.com/image/play.png">
    <meta name="author" content="techsolutions">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="UTF-8">
    <title>IRT</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.eot" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.svg" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.ttf" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.woff" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="http://irtbd.com/css/linearicons.css">
    <link rel="stylesheet" href="http://irtbd.com/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://irtbd.com/css/bootstrap.css">
    <link rel="stylesheet" href="http://irtbd.com/css/magnific-popup.css">
    <link rel="stylesheet" href="http://irtbd.com/css/nice-select.css">
    <link rel="stylesheet" href="http://irtbd.com/css/animate.min.css">
    <link rel="stylesheet" href="http://irtbd.com/css/owl.carousel.css">
    <link rel="stylesheet" href="http://irtbd.com/css/jquery-ui.css">
    <link rel="stylesheet" href="http://irtbd.com/css/main.css">
</head>

<body>
    <header id="header" id="home">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
                        <a href="tel:+953 012 3654 896"><span class="lnr lnr-phone-handset"></span> <span class="text">+88 02 9355114</span></a>
                        <a href="mailto:support@campustocorporate.com"><span class="lnr lnr-envelope"></span> <span class="text">info@irtbd.com</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container main-menu">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">

                    <h2>IRT <span>Institute of Research & Training</span></h2>

                </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li><a href="http://irtbd.com/index">Home</a></li>
                        <li class="menu-has-children"><a href="#">Courses</a>
                            <!--<ul>-->
                            <!--	<li><a href="#">Programming PHP</a></li>		-->
                            <!--	<li><a href="#">Networking</a></li>		-->
                            <!--	<li><a href="#">Graphic Design</a></li>				                		-->
                            <!--	<li><a href="#">Modern Javascript</a></li>				                		-->
                            <!--</ul>-->

                            <div id='megaMenu'>

                                <div class='singleMegaCourse' onclick="window.location.href='http://irtbd.com/course/1'">
                                    <div class='singmegcIcon'>
                                        <img src='http://irtbd.com/image/course/1568637242reading.png'>
                                    </div>
                                    <div class='singmegcTitleInfo'>
                                        <h3>Schooling For Students</h3>

                                    </div>

                                    <ol class='megaMenuSub'>
                                        <li><a href="http://irtbd.com/course_details/1">Market Readiness assessment </a></li>
                                        <li><a href="http://irtbd.com/course_details/2">Employability </a></li>
                                        <li><a href="http://irtbd.com/course_details/3">Workplace Communication &amp; Business English </a></li>
                                        <li><a href="http://irtbd.com/course_details/4">Arduino : Robotics </a></li>
                                        <li><a href="http://irtbd.com/course_details/5">Course of BigData </a></li>
                                        <li><a href="http://irtbd.com/course_details/6">Course on Data Science </a></li>
                                        <li><a href="http://irtbd.com/course_details/7">Course on AI </a></li>
                                        <li><a href="http://irtbd.com/course_details/8">Medical Scribe </a></li>
                                        <li><a href="http://irtbd.com/course_details/9">Financial Accounting and BPO </a></li>
                                        <li><a href="http://irtbd.com/course_details/10">Life skill and Life Style </a></li>
                                        <li><a href="http://irtbd.com/course_details/11">Creative Art </a></li>
                                        <li><a href="http://irtbd.com/course_details/12">Digital Marketing </a></li>
                                        <li><a href="http://irtbd.com/course_details/13">SEO </a></li>
                                        <li><a href="http://irtbd.com/course_details/14">And more Basic </a></li>
                                    </ol>
                                </div>
                                <div class='singleMegaCourse' onclick="window.location.href='http://irtbd.com/course/2'">
                                    <div class='singmegcIcon'>
                                        <img src='http://irtbd.com/image/course/1568637382student1.png'>
                                    </div>
                                    <div class='singmegcTitleInfo'>
                                        <h3>Schooling for Professionals</h3>

                                    </div>

                                    <ol class='megaMenuSub'>
                                        <li><a href="http://irtbd.com/course_details/15">Workplace Communication &amp; Business English </a></li>
                                        <li><a href="http://irtbd.com/course_details/16">Professional Course on digital content management </a></li>
                                        <li><a href="http://irtbd.com/course_details/17">Professional Graphics Design </a></li>
                                        <li><a href="http://irtbd.com/course_details/18">Professional Motion Graphics </a></li>
                                        <li><a href="http://irtbd.com/course_details/19">Web Development </a></li>
                                        <li><a href="http://irtbd.com/course_details/20">Future Leaders Talent  Development Program </a></li>
                                        <li><a href="http://irtbd.com/course_details/21">Foundations for Business Leadership Program </a></li>
                                        <li><a href="http://irtbd.com/course_details/22">Advanced Management program </a></li>
                                        <li><a href="http://irtbd.com/course_details/23">Transition to Business Leadership Program </a></li>
                                        <li><a href="http://irtbd.com/course_details/24">Senior Executive Management program </a></li>
                                        <li><a href="http://irtbd.com/course_details/25">Orchestrating Winning Performance </a></li>
                                        <li><a href="http://irtbd.com/course_details/26">And more </a></li>
                                        <li><a href="http://irtbd.com/course_details/55">Sales &amp; Marketing  Essential </a></li>
                                        <li><a href="http://irtbd.com/course_details/56">IT Support &amp; Help Desk Professional </a></li>
                                        <li><a href="http://irtbd.com/course_details/58">Digital Marketing &amp; SEO </a></li>
                                    </ol>
                                </div>
                                <div class='singleMegaCourse' onclick="window.location.href='http://irtbd.com/course/3'">
                                    <div class='singmegcIcon'>
                                        <img src='http://irtbd.com/image/course/1568637416manager.png'>
                                    </div>
                                    <div class='singmegcTitleInfo'>
                                        <h3>Schooling for Entrepreneurs</h3>

                                    </div>

                                    <ol class='megaMenuSub'>
                                        <li><a href="http://irtbd.com/course_details/27">Creativity &amp; Entrepreneurship </a></li>
                                        <li><a href="http://irtbd.com/course_details/28">Financial Analysis for Decision Making </a></li>
                                        <li><a href="http://irtbd.com/course_details/29">Becoming an Entrepreneur </a></li>
                                        <li><a href="http://irtbd.com/course_details/30">Building and Leading Effective Teams </a></li>
                                        <li><a href="http://irtbd.com/course_details/31">The Essential Guide to Entrepreneurship </a></li>
                                        <li><a href="http://irtbd.com/course_details/33">The Complete Product Management Course </a></li>
                                        <li><a href="http://irtbd.com/course_details/34">Introduction to Web Development: HTML </a></li>
                                        <li><a href="http://irtbd.com/course_details/35">Startup School </a></li>
                                        <li><a href="http://irtbd.com/course_details/36">Business laws and regulations </a></li>
                                        <li><a href="http://irtbd.com/course_details/37">Management principles </a></li>
                                        <li><a href="http://irtbd.com/course_details/38">Organizational behavior </a></li>
                                        <li><a href="http://irtbd.com/course_details/39">Entrepreneurial Finance </a></li>
                                        <li><a href="http://irtbd.com/course_details/40">SEO Training Course: Building Sustainable Traffic for Business Growth </a></li>
                                        <li><a href="http://irtbd.com/course_details/41">Financial Engineering and Risk Management </a></li>
                                        <li><a href="http://irtbd.com/course_details/42">Private Equity and Venture Capital from Bocconi </a></li>
                                        <li><a href="http://irtbd.com/course_details/43">Effective Fundraising and Leadership in Arts and Culture </a></li>
                                        <li><a href="http://irtbd.com/course_details/44">Corporate Entrepreneurship: Innovating within Corporations Specialization </a></li>
                                        <li><a href="http://irtbd.com/course_details/45">Branding, Content, and Social Media </a></li>
                                        <li><a href="http://irtbd.com/course_details/46">And more </a></li>
                                        <li><a href="http://irtbd.com/course_details/57">In-House Training </a></li>
                                    </ol>
                                </div>
                                <div class='singleMegaCourse' onclick="window.location.href='http://irtbd.com/course/4'">
                                    <div class='singmegcIcon'>
                                        <img src='http://irtbd.com/image/course/1568637471student.png'>
                                    </div>
                                    <div class='singmegcTitleInfo'>
                                        <h3>Industrial Attachment 2020</h3>

                                    </div>

                                    <ol class='megaMenuSub'>
                                        <li><a href="http://irtbd.com/course_details/47">Web App Development in ASP.NET </a></li>
                                        <li><a href="http://irtbd.com/course_details/48">Web Design &amp; Development with PHP &amp; MySql </a></li>
                                        <li><a href="http://irtbd.com/course_details/49">Cisco Certified Network Associate </a></li>
                                        <li><a href="http://irtbd.com/course_details/51">Computer Hardware &amp; Networking </a></li>
                                        <li><a href="http://irtbd.com/course_details/52">ISP Network Administration with Mikrotik </a></li>
                                        <li><a href="http://irtbd.com/course_details/53">Networking with Windows Server 2016 </a></li>
                                        <li><a href="http://irtbd.com/course_details/54">Linux System Administration RHEL 7.x </a></li>
                                    </ol>
                                </div>
                                <div class='singleMegaCourse' onclick="window.location.href='http://irtbd.com/course/6'">
                                    <div class='singmegcIcon'>
                                        <img src='http://irtbd.com/image/course/1569927797lecture.png'>
                                    </div>
                                    <div class='singmegcTitleInfo'>
                                        <h3>Schooling for special necessity</h3>

                                    </div>

                                    <ol class='megaMenuSub'>
                                        <li><a href="http://irtbd.com/course_details/59">Online: who living outside  of Dhaka </a></li>
                                    </ol>
                                </div>
                            </div>
                        </li>
                        <li><a href="http://irtbd.com/about">About</a></li>
                        <!--<li><a href="#">Events</a></li>-->
                        <!--<li><a href="#">Gallery</a></li>-->
                        <!--<li><a href="#">Blog</a></li>	-->

                        <li><a href="http://irtbd.com/contact">Contact</a></li>
                    </ul>
                </nav>
                <!-- #nav-menu-container -->
                <a href='http://irtbd.com/student_apply' id='applyNow'>Apply Now</a>
            </div>
        </div>
    </header>
    <!-- start banner Area -->
    <!-- End banner Area -->
    <!-- Start feature Area -->
    <!--Main content-->
    <div class="pageBradcamb" style="background-image:url(http://irtbd.com/image/subcourse/1568694262marketAssessments.jpg)">
        <div class="bradcambContent">
            <h2>Market Readiness assessment</h2>

        </div>
    </div>

    <div class="courserDescriptSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="courseDtlOutlineSection">
                        <div class="course-at-glance-wrapper-container">
                            <h4 class="coursr-glance"> course at a glance </h4>
                            <ul class="couser-duration">
                                <li> <span><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Date :</span> 9 Oct - 30 Dec 2019 </li>
                                <li> <span><i class="fa fa-list" aria-hidden="true"></i>No. of Classes/ Sessions :</span> 30 </li>
                                <li> <span><i class="fa fa-clock-o" aria-hidden="true"></i>Total Hours :</span> 90 </li>
                                <li> <span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>Last Date of Registration :</span> 6 Oct 2019 </li>
                                <li style="display: block"> <span><i class="fa fa-clock-o" aria-hidden="true"></i>Class Schedule :</span>
                                    <br>
                                    <ul class="courser-schdule-wrape">
                                        <li class="schdule-time-stamp">
                                            Monday - 7:00 PM - 9:30 PM </li>
                                        <li class="schdule-time-stamp">
                                            Wednesday - 7:00 PM - 9:30 PM </li>
    
                                    </ul>
                                </li>
    
                                <li>
                                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>venue :</span>20/2, West Panthopath, Ground Floor Dhaka-1205, Bangladesh
                                    <a style="margin-top: 4px; color: #fff" class="extra-link" href="https://www.google.com.bd/maps/place/Good+Luck+Center%2C+151%2F7%2C+Panthapath+Signal%2C+Green+Road%2C+7th+Floor%2C+Dhaka+1205%2C+Bangladesh" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
    
                        <div class="courseOutlineSection">
                            <div class="top_wrape course-berief">
                                <div class="course-berief-details">
                                    <p><span style="font-size: 10pt;">This training is jointly organized by Techsolutions and nextech ltd</span></p>
                                    <p><span style="font-size: 10pt;">Python is a powerful and flexible object-oriented scripting language that is designed for ease of use and enhanced productivity. In this training course, you gain the skills to achieve rapid development cycles, faster time-to-market, and lower cost of maintenance by developing applications using Python's language features, standard library modules, and third-party software packages.</span></p>
                                    <p><span style="font-size: 10pt;"><strong>Participants will learn: </strong></span></p>
                                    <ul type="disc">
                                        <li><span style="font-size: 10pt;">Rapidly develop feature-rich applications from Python's built-in statements, functions, and collection types</span></li>
                                        <li><span style="font-size: 10pt;">Structure code with classes, modules, and packages that leverage OO features</span></li>
                                        <li><span style="font-size: 10pt;">Create multiple data accessors to manage various data storage formats</span></li>
                                        <li><span style="font-size: 10pt;">Develop dynamic, platform-independent GUIs and data-driven web applications</span></li>
                                    </ul>
                                    <h2 class="event_title"><span style="font-size: 12pt;">Course Outline</span></h2>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>Course Outline</strong></span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>1 – An Introduction to Python.</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> A Brief History of Python Python Versions</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Installing Python</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Environment Variables</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Executing Python from the Command Line</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> IDLE</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Editing Python Files</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Python Documentation</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Getting Help</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Dynamic Types</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Python Reserved Words</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Naming Conventions</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>2 – Basic Python Syntax</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Instruct</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Basic Syntax Comments</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> String Values</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> String Methods</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The format Method</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> String Operators</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Numeric Data Types</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Conversion Functions</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Simple Input and Output The % Method</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The print Function</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>3 – Language Components</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Indenting Requirements</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The if Statement</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Relational Operators</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Logical Operators</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Bit Wise Operators</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The while Loop</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> break and continue</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The for Loop</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>4 – Collections</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Lists</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Tuples Sets Dictionaries</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Sorting Dictionaries</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Copying Collections</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>5 – Functions</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Defining Your Own Functions</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Parameters</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Function Documentation</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Keyword and Optional</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Parameters</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Passing Collections to a Function</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Variable Number of Arguments Scope</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Functions – “First Class Citizens”</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Passing Functions to a Function</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Mapping Functions in a Dictionary</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Lambda</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Inner Functions</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Closures</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>6 – Modules</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Modules</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Standard Modules – sys</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Standard Modules – math</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Standard Modules – time</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The dir Function</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>7 – Input and Output</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Data Streams</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Creating Your Own Data Streams</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Access Modes</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Writing Data to a File</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Reading Data From a File</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Additional File Methods</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Using Pipes as Data Streams</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Handling IO Exceptions</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Working with Directories Metadata</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The pickle Module</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>8.Implementing Classes and Objects…. OOP1</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Declaring and modifying objects</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Encapsulating attributes and methods in classes</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Initializing objects with constructors</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Accessing and modifying attributes with methods</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Overloading operators and polymorphism</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>9 – OOP2 + Exceptions</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Inheritance</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Reusing functionality through inheritance</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Extending methods from base classes</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Overriding methods for dynamic behavior</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Tracing the scope in the namespace</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Enhancing functionality with class decorators</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Errors</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Run Time Errors</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> The Exception Model</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Exception Hierarchy</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Handling Multiple Exceptions raise</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> assert</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Writing Your Own Exception Classes</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>10 Database</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Database concepts</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Database design</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> SQL</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Connecting Database with raw python</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>11 Django 1</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Creating virtual environment</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Installing Django</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Creating a Project</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Creating Our First App</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Overview of a Basic App</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>12 Django 2</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Database Setup</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Creating Models</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Activating Models</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Admin Interface</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>13 Django 2</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> URLS</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Views</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> ORM</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Filtering Database Results</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>14 Django 3</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Templates</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Creating a Base Template</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Raising a 404 HTTP Error</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Related Objects Set</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Designing the Details Template</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Bootstrap and Static Files</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>15 Django 4</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Removing Hardcoded URLs</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Adding data to our Database</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>16 Django 2</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Simple Form</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Adding Forms to the Template</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Favorite View Function</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Model Forms</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> ModelForm and CreateView</span>
                                        <br><span style="color: #000000; font-size: 10pt;"> UpdateView and DeleteView</span></p>
                                    <p><span style="color: #000000; font-size: 10pt;"><strong>17 Django 2</strong></span>
                                        <br><span style="color: #000000; font-size: 10pt;"> Exam and Project View</span></p>
                                    <!--<samp class="see-more-wrapper"><a class="see-more" href="javascript:void(0)"><i class="fa fa-plus-circle" aria-hidden="true"></i> See more</a> </samp>-->
                                    <div class="clearfix"></div>
                                    <h3>Curriculum </h3>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Module</td>
                                                    <td>Core Python Programming</td>
                                                    <td>50 Hrs</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                <div class="courseDetailsSidebar">
                    <div class="singleWidget">
                        <h3 class="widgetTitle">TENTATIVE CLASS START</h3>
                        <p class="classStartDate">15 October 2019</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">AVAILABLE SEAT</h3>
                        <p class="availableSeat">10 / 20</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">WHO CAN JOIN</h3>
                        <p>Graduate</p>
                        <p>Diploma Complated Student</p>
                        <p>Entry Level Executives</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">Training Vanue</h3>
                        <p class="traniningVanueLoc">20/2, West Panthopath, Ground Floor Dhaka-1205, Bangladesh</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">Meet the Trainer</h3>
                        <div class="trainerImg">
                            <img src="https://techsolutionsbd.com/images/team/ceo.jpg" />
                            <p>Naznin Nahar</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function openModal(id, sub_ids) {
            var subid = parseInt(sub_ids);

            document.getElementById('studentApplyFormPopup').classList.add('active');
            $('#course_details>option:eq(' + id + ')').attr('selected', true);
            var course_id = id;
            $.get('/course_details/fetch?course_id=' + course_id, function(data) {
                $('#subcourse_details').empty();
                $.each(data, function(student_apply, subcourseObj) {
                    var selected = '';
                    if (subcourseObj.id == subid) {
                        selected = 'selected';
                    }
                    $('#subcourse_details').append('<option value="' + subcourseObj.id + '" ' + selected + '>' + subcourseObj.subcourse_name + '</option>');
                });

            });

            //$('#subcourse_details>option:eq('+ subid +')').attr('selected', true);  
        }
    </script>

    <!-- MAin content end here-->

    <!-- start footer Area -->
    <footer class="footer-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="single-footer-widget">

                        <h2 class="footerLogo">Institute of Research & Training</h2>
                        <p>One Of The Best It Training Institute In Bangladesh. We Make Professionals.</p>
                        <div class="footer-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="single-footer-widget">
                        <h4>Contact</h4>
                        <div class='footerContact'>
                            <div class='singleContact'>
                                <p><b>Location: </b>20/2, West Panthopath, Ground Floor Dhaka-1205, Bangladesh</p>
                                <p><b>Phone: </b>+88 02 9355114</p>
                                <p><b>Email: </b>info@irtbd.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom row align-items-center justify-content-between">
                <p class="footer-text m-0 col-lg-6 col-md-12">
                    Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script> All rights reserved </a>
                </p>

                <p class="mt-3">Design And Development by <a href="https://techsolutionsbd.com">Techsolutions</a></p>

            </div>
        </div>
    </footer>
    <!-- End footer Area -->

    <div class="col-md-8 offset-2" id='studentApplyFormPopup' name="studentApplyFormPopup">
        <span onclick="document.getElementById('studentApplyFormPopup').classList.remove('active')" class="close">x</span>
        <div class="contact-form-area">
            <form class="contact-from" action="http://irtbd.com/student_form" method="post" enctype="multipart/form-data">

                <input type="hidden" name="_token" value="7EpSh29EXnWj5VOEt9mong5aEQCrwlBJepILzUxm">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="name" id="contact-name" placeholder="Full Name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="email" name="email" id="contact-website" placeholder="Email" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="number" name="number" id="contact-email" placeholder="Contact Number" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="address" id="contact-email" placeholder="Address" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control dynamic" name="course" id="course_details" required>
                                <option value="">Choose a course</option>

                                <option value="1">Schooling For Students</option>
                                <option value="2">Schooling for Professionals</option>
                                <option value="3">Schooling for Entrepreneurs</option>
                                <option value="4">Industrial Attachment 2020</option>
                                <option value="6">Schooling for special necessity</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control" name="subcourse" id="subcourse_details" required>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="qualification" id="contact-email" placeholder="Last Education Qualification" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="institute" id="contact-email" placeholder="Institute Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <span style="color: black; ">Birthdate:</span>
                            <input type="date" name="birthdate" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span style="color: black; ">Student Image:</span>
                            <div class="custom-file">
                                <input type="file" name="image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </div>
            </form>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Drag and drop a file here or click',
                    replace: 'Drag and drop a file or click to replace',
                    remove: 'Remove',
                    error: 'Sorry, the file is too large'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            drEvent.on('dropify.error.fileSize', function(event, element) {
                alert('Filesize error message!');
            });
            drEvent.on('dropify.error.minWidth', function(event, element) {
                alert('Min width error message!');
            });
            drEvent.on('dropify.error.maxWidth', function(event, element) {
                alert('Max width error message!');
            });
            drEvent.on('dropify.error.minHeight', function(event, element) {
                alert('Min height error message!');
            });
            drEvent.on('dropify.error.maxHeight', function(event, element) {
                alert('Max height error message!');
            });
            drEvent.on('dropify.error.imageFormat', function(event, element) {
                alert('Image format error message!');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.assets/js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="http://irtbd.com/js/bootstrap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="http://irtbd.com/js/easing.min.js"></script>
    <script src="http://irtbd.com/js/hoverIntent.js"></script>
    <script src="http://irtbd.com/js/superfish.min.js"></script>
    <script src="http://irtbd.com/js/jquery.ajaxchimp.min.js"></script>
    <script src="http://irtbd.com/js/jquery.magnific-popup.min.js"></script>
    <script src="http://irtbd.com/js/jquery.tabs.min.js"></script>
    <script src="http://irtbd.com/js/jquery.nice-select.min.js"></script>
    <script src="http://irtbd.com/js/owl.carousel.min.js"></script>
    <script src="http://irtbd.com/js/mail-script.js"></script>
    <script src="http://irtbd.com/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
    <script>
        $('#course').on('change', function(e) {
            var course_id = e.target.value;

            $.get('student/fetch?course_id=' + course_id, function(data) {
                $('#subcourse').empty();
                $.each(data, function(student_apply, subcourseObj) {
                    $('#subcourse').append('<option value="' + subcourseObj.id + '">' + subcourseObj.subcourse_name + '</option>');
                })

            });
        })
        $('#course_details').on('change', function(e) {
            var course_id = e.target.value;

            $.get('/course_details/fetch?course_id=' + course_id, function(data) {
                $('#subcourse_details').empty();
                $.each(data, function(student_apply, subcourseObj) {
                    $('#subcourse_details').append('<option value="' + subcourseObj.id + '">' + subcourseObj.subcourse_name + '</option>');
                })

            });
        })
    </script>
</body>

</html>