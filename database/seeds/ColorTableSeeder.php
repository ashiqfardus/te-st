<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Color::create([
           'theme_primary'=>'#545545',
           'theme_secondary'=>'#564556',
            'header_footer'=>'#dsfxhzjkfv'
        ]);
    }
}
