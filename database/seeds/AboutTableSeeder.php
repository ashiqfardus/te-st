<?php

use Illuminate\Database\Seeder;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\About::create([
           'description'=>'The chaos is a prime ego.Shake six rhubarbs, truffels, and thyme in a large jar over medium heat, sauté for fifteen minutes and rinse with some peanuts.Golly gosh, yer not pulling me without a grace!'
        ]);
    }
}
