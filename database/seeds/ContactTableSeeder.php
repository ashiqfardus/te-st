<?php

use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Contact::create([
           'address'=>'404 golam rasul plaza',
            'phone'=>54512215152,
            'mobile'=>54555546,
            'email'=>'ashiqfardus@hotmail.com'
        ]);
    }
}
