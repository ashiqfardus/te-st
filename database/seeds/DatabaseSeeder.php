<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(LogoTableSeeder::class);
         $this->call(ContactTableSeeder::class);
         $this->call(ColorTableSeeder::class);
         $this->call(SocialTableSeeder::class);
         $this->call(AboutTableSeeder::class);
         $this->call(UserTableSeeder::class);
    }
}
