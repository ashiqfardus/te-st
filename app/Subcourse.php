<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcourse extends Model
{
   protected $fillable=['subcourse_name','course_id','details','popular','image','active_status','ongoing','start_date','end_date', 
                        'session_no', 'total_hours', 'class_schedule', 'venue','price','tentative_class','total_seat',
                        'available_seat','join_requirement', 'trainer_name','trainer_image'];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
