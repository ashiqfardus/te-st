<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=['name','email','number','address','course','birthdate','image','qualification','institute'];
}
