<?php

namespace App\Http\Controllers;
use App\Course;
use App\Subcourse;
use Session;
use Illuminate\Http\Request;

class SubCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $n=1;
        $course=Course::all();

        return view('admin.subcourse.index')->with('subcourses',Subcourse::all())->with('n',$n)->with('course_name',$course);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.subcourse.create')->with('courses',Course::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'subcourse_name'=>'required',
           'course_id' => 'required',
            'image'=>'required|image'
        ]);
        $image=$request->image;
        $image_new_name=time().$image->getClientOriginalName();
        $image->move('image/subcourse',$image_new_name);

        Subcourse::create([
           'subcourse_name'=>$request->subcourse_name,
           'course_id'=>$request->course_id,
           'image'=>$image_new_name,
           'details'=>$request->details,
           'course_outline'=>$request->course_outline,
          'trainer_name'=>$request->trainer_name,
          'start_date'=>$request->start_date,
          'end_date'=>$request->end_date,
          'class_schedule'=>$request->class_schedule,
          'tentative_class'=>$request->tentative_class,
          'total_seat'=>$request->total_seat,
          'available_seat'=>$request->available_seat,
          'session_no'=>$request->session_no,
          'total_hours'=>$request->hours,
          'price'=>$request->price,
          'venue'=>$request->venue,
          'join_requirement'=>$request->join_requirement
        ]);
        Session::flash('success','Sub-Course has been added');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcourse=Subcourse::find($id);
        return view('admin.subcourse.edit')->with('subcourse',$subcourse)->with('courses',Course::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subcourse=Subcourse::find($id);
        $this->validate($request,[
            'subcourse_name'=>'required',
            'course_id' => 'required'
        ]);

        if ($request->hasFile('image'))
        {
            $image=$request->image;
            $image_new_name=time().$image->getClientOriginalName();
            $image->move('image/subcourse',$image_new_name);
            $subcourse->image=$image_new_name;
        }
        
        if ($request->hasFile('trainer_image'))
        {
            $image=$request->trainer_image;
            $image_new_name=time().$image->getClientOriginalName();
            $image->move('image/subcourse',$image_new_name);
            $subcourse->trainer_image=$image_new_name;
        }

          $subcourse->subcourse_name=$request->subcourse_name;
          $subcourse->course_id=$request->course_id;
          $subcourse->trainer_name=$request->trainer_name;
          $subcourse->start_date=$request->start_date;
          $subcourse->end_date=$request->end_date;
          $subcourse->class_schedule=$request->class_schedule;
          $subcourse->tentative_class=$request->tentative_class;
          $subcourse->total_seat=$request->total_seat;
          $subcourse->available_seat=$request->available_seat;
          $subcourse->session_no=$request->session_no;
          $subcourse->total_hours=$request->hours;
          $subcourse->price=$request->price;
          $subcourse->venue=$request->venue;
          $subcourse->join_requirement=$request->join_requirement;
          $subcourse->details=$request->details;
          $subcourse->course_outline=$request->course_outline;
          $subcourse->save();
        Session::flash('success','Sub-Course has been Updated');
        return redirect()->route('sub-course.index');
    }

    public function popular($id)
    {
        $subcourse=Subcourse::find($id);
        if ($subcourse->popular==0)
        {
            $subcourse->popular=1;
        }
        else
        {
            $subcourse->popular=0;
        }
        $subcourse->save();
        Session::flash('success','Sub-Course has been Updated');
        return redirect()->route('sub-course.index');
    }
    
    public function active_status($id)
    {
        $subcourse=Subcourse::find($id);
        if ($subcourse->active_status==0)
        {
            $subcourse->active_status=1;
        }
        else
        {
            $subcourse->active_status=0;
        }
        $subcourse->save();
        Session::flash('success','Sub-Course has been Updated');
        return redirect()->route('sub-course.index');
    }
    public function ongoing($id)
    {
        $subcourse=Subcourse::find($id);
        if ($subcourse->ongoing==0)
        {
            $subcourse->ongoing=1;
        }
        else
        {
            $subcourse->ongoing=0;
        }
        $subcourse->save();
        Session::flash('success','Sub-Course has been Updated');
        return redirect()->route('sub-course.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcourse=Subcourse::find($id);
        $subcourse->delete();
        unlink('image/subcourse/'.$subcourse->image);
        Session::flash('success','Sub-Course Deleted successfully');
        return redirect()->back();
    }
}
