<?php

namespace App\Http\Controllers;
use App\Subcourse;
use Session;
use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $n=1;
        $subcourse=Subcourse::all();
        return view('admin.student.index')->with('students',Student::all())->with('n',$n)->with('subcourses',$subcourse);
    }

    public function delete($id)
    {
        $student=Student::find($id);
        $student->delete();
        unlink('image/student/'.$student->image);
        Session::flash('success','Student Deleted successfully');
        return redirect()->back();
    }
}
