<?php

namespace App\Http\Controllers;
use Session;
use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $n=1;
         return view('admin.testimonial.index')->with('testimonial',Testimonial::all())->with('n',$n);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'image'=>'required|image',
            'details'=>'required'
        ]);

        $image=$request->image;
        $image_new_name=time().$image->getClientOriginalName();
        $image->move('image/testimonial',$image_new_name);

        $testimonial=Testimonial::create([
            'name'=>$request->name,
            'image'=>$image_new_name,
            'details'=>$request->details
        ]);

        Session::flash('success','Testimonial has been added');

        return redirect()->route('testimonial.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial=Testimonial::find($id);
        return view('admin.testimonial.edit')->with('testimonial',$testimonial);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimonial=Testimonial::find($id);
        $this->validate($request,[
            'name'=>'required',
            'details'=>'required'
        ]);
        if ($request->hasFile('image'))
        {
            $image=$request->image;
            $image_new_name=time().$image->getClientOriginalName();
            $image->move('image/testimonial',$image_new_name);
            $testimonial->image=$image_new_name;
        }

        $testimonial->name=$request->name;

        $testimonial->details=$request->details;
        $testimonial->save();
        Session::flash('success','Testimonial has been Updated');
        return redirect()->route('testimonial.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial=Testimonial::find($id);
        $testimonial->delete();
        unlink('image/testimonial/'.$testimonial->image);
        Session::flash('success','Testimonial Deleted successfully');
        return redirect()->back();
    }
}
