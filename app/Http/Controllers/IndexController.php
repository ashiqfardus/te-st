<?php

namespace App\Http\Controllers;
use App\Logo;
use http\Message;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Teacher;
use Session;
use App\Contact;
use App\Course;
use App\Slider;
use App\Student;
use App\Subcourse;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome')
            ->with('sliders',Slider::first()->image)
            ->with('contacts',Contact::first())
            ->with('course',Course::all())
            ->with('subcourses',Subcourse::all()->where('active_status',1))
            ->with('testimonial',Testimonial::all())
            ->with('logo',Logo::first())
            ->with ('ongoing',Subcourse::all()->where('ongoing',1)->take(4));
    }

    public function course_page($id)
    {
       $subcourses=Subcourse::all()->where('course_id',$id);

        return view('course')
            ->with('allcourse',$subcourses)
            ->with('contacts',Contact::first())
            ->with('course',Course::all())
            ->with('subcourses',Subcourse::all())
            ->with('logo',Logo::first());
    }

     public function course_details($id)
    {
       $subcourses=Subcourse::find($id);

        return view('course_details')
            ->with('details',$subcourses)
            ->with('id',Course::all())
            ->with('contacts',Contact::first())
            ->with('course',Course::all())
            ->with('course_id',Course::all())
            ->with('subcourses',Subcourse::all())
            ->with('logo',Logo::first());
    }

 
    public function contact()
    {
        return view('contact')
            ->with('contacts',Contact::first())
            ->with('course',Course::all())
            ->with('subcourses',Subcourse::all())
            ->with('logo',Logo::first());
    }
    public function student_apply()
    {
        return view('student_apply')
            ->with('course_name',Course::all())
            ->with('contacts',Contact::first())
            ->with('course',Course::all())
            ->with('subcourses',Subcourse::all())
            ->with('logo',Logo::first());
    }

    function student_fetch(Request $request)
    {
       $course_id=Input::get('course_id');
        $subcourse=Subcourse::where('course_id','=',$course_id)->get();
        return Response::json($subcourse);
    }



    public function student_form(Request $request)
    {
        $this->validate($request,[
           'name'=>'required',
           'email'=>'required',
           'number'=>'required',
           'address'=>'required',
           'subcourse'=>'required',
           'birthdate'=>'required',
           'image'=>'required|image',
           'qualification'=>'required',
           'institute'=>'required',
        ]);

        $image=$request->image;
        $image_new_name=time().$image->getClientOriginalName();
        $image->move('image/student',$image_new_name);


        Student::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'number'=>$request->number,
            'address'=>$request->address,
            'course'=>$request->subcourse,
            'birthdate'=>$request->birthdate,
            'image'=>$image_new_name,
            'qualification'=>$request->qualification,
            'institute'=>$request->institute,
        ]);

        return redirect()->back()->with('alert','Registration Complete');
    }

    public function teacher_apply()
    {
        return view('teacher_apply')
            ->with('contacts',Contact::first())
            ->with('course',Course::all())
            ->with('subcourses',Subcourse::all())
            ->with('logo',Logo::first());
    }

    public function teacher_form(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'number'=>'required',
            'address'=>'required',
            'cv'=>'required|mimes:doc,pdf,docx',
        ]);
        $cv=$request->cv;
        $cv_new_name=time().$cv->getClientOriginalName();
        $cv->move('file/',$cv_new_name);


        Teacher::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'number'=>$request->number,
            'address'=>$request->address,
            'cv'=>$cv_new_name,
        ]);

        return redirect()->back()->with('alert','Registration Complete');
    }

    public function mail(Request $request)
    {
        $data=array(
            'name'=>$request->name,
            'subject'=>$request->subject,
            'email'=>$request->email,
            'message'=>$request->message
        );

        Mail::to('info@irtbd.com')->cc('nazninnhr09@gmail.com')->send(new SendMail($data));
        return redirect()->back()->with('alert','Message has been sent');
        // return view('contact_send')->with('data',$request);
    }

}
