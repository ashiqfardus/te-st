<?php

namespace App\Http\Controllers;
use Session;
use App\Teacher;
use Illuminate\Http\Request;

class InstructorController extends Controller
{
    public function index()
    {
        $n=1;
        return view('admin.instructor.index')->with('instructors',Teacher::all())->with('n',$n);
    }

    public function delete($id)
    {
        $instructor=Teacher::find($id);
        $instructor->delete();
        unlink('file/'.$instructor->cv);
        Session::flash('success','Instructor Deleted successfully');
        return redirect()->back();
    }
}
