<?php

namespace App\Http\Controllers;
use Session;
use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $n=1;
        return view('admin.course.index')->with('courses',Course::all())->with('n',$n);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.course.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'course_name'=>'required',
           'icon'=>'required|image'
        ]);

        $image=$request->icon;
        $image_new_name=time().$image->getClientOriginalName();
        $image->move('image/course',$image_new_name);

        $course=Course::create([
           'course_name'=>$request->course_name,
           'icon'=>$image_new_name
        ]);

        Session::flash('success','Course has been added');

        return redirect()->route('course.create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course=Course::find($id);
        return view('admin.course.edit')->with('course',$course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course=Course::find($id);
        $this->validate($request,[
            'course_name'=>'required'
        ]);
        if ($request->hasFile('icon'))
        {
            $image=$request->icon;
            $image_new_name=time().$image->getClientOriginalName();
            $image->move('image/course',$image_new_name);
            $course->icon=$image_new_name;
        }

        $course->course_name=$request->course_name;
        $course->save();
        Session::flash('success','Course has been Updated');
        return redirect()->route('course.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course=Course::find($id);
        $course->delete();
        unlink('image/course/'.$course->icon);
        Session::flash('success','Course Deleted successfully');
        return redirect()->back();
    }
}
