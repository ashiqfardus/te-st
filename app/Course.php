<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable=['course_name','icon'];

    public function subcourses()
    {
        return $this->hasMany('App\Subcourse');
    }
}
