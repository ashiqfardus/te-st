@extends('layouts.main')

@section('content')
    <div class="pageBradcamb"  style="background-image:url({{asset('/image/subcourse/'.$details->image)}})">
        <div class="bradcambContent">
            <h2>{{$details->subcourse_name}}</h2>
            
        </div>
    </div>


<?php 
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $path = parse_url($actual_link, PHP_URL_PATH);
    $pathFragments = explode('/', $path);
    $token = end($pathFragments);
?>

    <div class="contactMain" style="padding:50px;">
        <div class="container">

                <!--{!! $details->details!!}-->
                <div class="courserDescriptSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="courseDtlOutlineSection">
                        <div class="course-at-glance-wrapper-container">
                            <h4 class="coursr-glance"> course at a glance </h4>
                            <ul class="couser-duration">
                                <li> <span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>Date :</span> {{ date('d-M-y', strtotime($details->start_date)) }} - {{ date('d-M-y', strtotime($details->end_date)) }} </li>
                                <li> <span><i class="fa fa-list" aria-hidden="true"></i>No. of Classes/ Sessions :</span> {{ $details->session_no }} </li>
                                <li> <span><i class="fa fa-clock-o" aria-hidden="true"></i>Total Hours :</span> {{ $details->total_hours }} </li>
                                <li style="display: block"> <span><i class="fa fa-clock-o" aria-hidden="true"></i>Class Schedule : {{ $details->class_schedule }}</span></li>
    
                                <li>
                                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>Venue :</span>{{ $details->venue }}
                                </li>
                                <li>Course Fee: <span><b>{{ $details->price }} BDT</b></span> </li>
                            </ul>
                            <button onclick="openModal({{$details->course_id}},{{$details->id}})" class="btn btn-primary" id='popupStdForm'>Apply Now</button>
                        </div>
    
                        <div class="courseOutlineSection">
                            <div class="top_wrape course-berief">
                                <div class="course-berief-details">
                                    
                                    <h3 class="courseOverview">Course Overview</h3>
                                    {!! $details->details !!}
                                    <h3 class="couseOutline">Course Outline</h3>
                                    {!! $details->course_outline !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                <div class="courseDetailsSidebar">
                    <div class="singleWidget">
                        <h3 class="widgetTitle">TENTATIVE CLASS START</h3>
                        <p class="classStartDate">{{ date('d-M-y', strtotime($details->tentative_class)) }}</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">AVAILABLE SEAT</h3>
                        <p class="availableSeat">{{ $details->available_seat }} / {{ $details->total_seat }}</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">WHO CAN JOIN</h3>
                        {!! $details->join_requirement !!}
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">Training Vanue</h3>
                        <img class="widgetVanueImg" src="{{asset('image/irt.png/')}}" />
                        <p class="traniningVanueLoc">{{ $details->venue }}</p>
                    </div>
                    <div class="singleWidget">
                        <h3 class="widgetTitle">Meet the Trainer</h3>
                        <div class="trainerImg">
                            <img src="{{asset('/image/subcourse/'.$details->trainer_image)}}" />
                            <p>{{ $details->trainer_name }}</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
                
                
      
        </div>
    </div>
    

<script>
function openModal(id,sub_ids){
   var subid= parseInt(sub_ids);
   
    document.getElementById('studentApplyFormPopup').classList.add('active');
   // $('#course_details>option:eq('+ id +')').attr('selected', true);
    var course_id =id;
     $.get('/course_details/fetch?course_id='+ course_id, function (data) {
            $('#subcourse_details').empty();
            $.each(data,function (student_apply, subcourseObj) {
				var	selected='';
				if(subcourseObj.id==subid){
					selected='selected';
					}
                $('#subcourse_details').append('<option value="'+subcourseObj.id +'" '+selected+'>'+subcourseObj.subcourse_name+'</option>');
            });
            
        });
		
		//$('#subcourse_details>option:eq('+ subid +')').attr('selected', true);  
}
</script>



<div class="col-md-8 offset-2" id='studentApplyFormPopup' name="studentApplyFormPopup">
                    <span onclick="document.getElementById('studentApplyFormPopup').classList.remove('active')" class="close">x</span>
                    @if (session('alert'))
                        <div class="alert alert-success">
                            {{ session('alert') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="contact-form-area">
                        <form class="contact-from" action="{{route('student_form')}}" method="post" enctype="multipart/form-data">

                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="name" id="contact-name" placeholder="Full Name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" name="email" id="contact-website" placeholder="Email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="number" name="number" id="contact-email" placeholder="Contact Number" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="address" id="contact-email" placeholder="Address" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control dynamic" name="course" id="course_details" required>
                                            <option value="">Choose a course</option>
        
                                            @foreach($course as $c)
                                            <option value="{{$c->id}}" <?php if($c->id==$details->course_id){ echo 'selected';}?>>{{$c->course_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <select class="form-control" name="subcourse" id="subcourse_details" required>
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="qualification" id="contact-email" placeholder="Last Education Qualification" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="institute" id="contact-email" placeholder="Institute Name" required>
                                    </div>
                                </div>
                            </div>
                            
                            
                           <div class="row">
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <span style="color: black; ">Birthdate:</span>
                                        <input type="date" name="birthdate" required>
                                    </div>
                               </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <span style="color: black; ">Student Image:</span>
                                        <div class="custom-file">
                                            <input type="file" name="image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true" required>
                                        </div>
                                    </div>
                               </div>
                           </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" > Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

    @endsection


















