@extends('admin.layout.app')
@section('content')

    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6  class="float-left m-0 font-weight-bold text-primary">Testimonial</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>CV</th>
                            <th>Action</th>

                        </tr>
                        </thead>

                        <tbody>

                        @foreach($instructors as $post)
                            <tr>
                                <td>{{$n++}}</td>
                                <td>{{$post->name}}</td>
                                <td>{{$post->email}}</td>
                                <td>{{$post->number}}</td>
                                <td>{{$post->address}}</td>
                                <td><a href="{{asset('file/'.$post->cv)}}"><i class="fa fa-file-pdf fa-2x"></i></a></td>

                                <td>
                                    <a href="{{route('instructor.delete',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

