
@extends('admin.layout.app')

@section('content')
    <h3 class="text-center mb-5 mt-5">Testimonial Add</h3>
    <div class="col-md-10 offset-md-1">
        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
        <form action="{{route('testimonial.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Full name" name="name">
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="custom-file">
                            <label>Choose file</label>
                            <input type="file" name="image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mt-2">
                    Details:
                    <textarea class="form-control" name="details"></textarea>
                </div>
                <div class="col-md-2 mt-2 text-center d-flex ml-auto">
                    <button type="submit" class="btn btn-primary"> Submit </button>
                </div>

            </div>
        </form>
    </div>
@endsection

