@extends('admin.layout.app')

@section('content')

    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6  class="float-left m-0 font-weight-bold text-primary">Testimonial</h6>
                <a class="float-right btn btn-primary btn-sm" href="{{route('testimonial.create')}}">Add New <i class="fa fa-plus"></i></a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th> Name</th>
                            <th>Image</th>
                            <th>Details</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($testimonial as $post)
                            <tr>
                                <td>{{$n++}}</td>
                                <td>{{$post->name}}</td>
                                <td><img src="{{asset('/image/testimonial/'.$post->image)}}" style="height: 80px; width: 80px;"></td>
                                <td>{!! $post->details !!}</td>
                                <td>
                                    <a href="{{route('testimonial.edit',['id'=>$post->id])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="{{route('testimonial.delete',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection
