@extends('admin.layout.app')
@section('content')

    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6  class="float-left m-0 font-weight-bold text-primary">Testimonial</h6>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Course</th>
                            <th>Image</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Birthdate</th>
                            <th>Address</th>
                            <th>Qualification</th>
                            <th>Institute</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($students as $post)
                            <tr>
                                <td>{{$n++}}</td>
                                <td>{{$post->name}}</td>
                                @foreach($subcourses as $s)
                                    @if($s->id==$post->course)
                                        <td>{{$s->subcourse_name}}</td>
                                    @endif
                                @endforeach

                                <td><img src="{{asset('/image/student/'.$post->image)}}" style="height: 80px; width: 80px;"></td>
                                <td>{{$post->email}}</td>
                                <td>{{$post->number}}</td>
                                <td>{{$post->birthdate}}</td>
                                <td>{{$post->address}}</td>
                                <td>{{$post->qualification}}</td>
                                <td>{{$post->institute}}</td>

                                <td>
                                    <a href="{{route('student.delete',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection

