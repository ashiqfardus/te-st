@extends('admin.layout.app')
@section('content')
    <h3 class="text-center mb-5 mt-5">Sub-Course Add</h3>
    <div class="col-md-12 ">
        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
        <form action="{{route('sub-course.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-row">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Course name" name="subcourse_name">
                </div>
                <div class="col-md-6 mt-2">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1" name="course_id">
                          
                            @foreach($courses as $course)
                                <option value="{{$course->id}}">{{$course->course_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 mt-2">
                    <input name="trainer_name" type="text" placeholder="Trainer Name" class="form-control" required>
                </div>
                
                <div class="col-md-6">
                    <label class="mb-0 mt-1">Start Date:</label>
                    <input name="start date" type="date" class="form-control" required>
                </div>
                <div class="col-md-6">
                    <label class="mb-0 mt-1">End Date:</label>
                    <input name="end date" type="date" class="form-control" required>
                </div>
                
                    <div class="col-md-6 mb-2">
                       <label>Class Schedule:</label>
                       <input name="class_schedule" type="text" class="form-control">
                   </div>
                   <div class="col-md-6 mb-2">
                       <label>Tentative Class:</label>
                       <input name="tentative_class" type="date" class="form-control">
                   </div>
                   <div class="col-md-6 mb-2">
                       <label>Total Seat:</label>
                       <input name="total_seat" type="text" class="form-control">
                   </div>
                   <div class="col-md-6 mb-2">
                       <label>Available Seat:</label>
                       <input name="available_seat" type="text" class="form-control">
                   </div>
            
                   <div class="col-md-4">
                       <label>No. of Sesion</label>
                       <input name="session_no" type="text" class="form-control">
                   </div>
                   <div class="col-md-4">
                       <label>Total Hours</label>
                       <input name="hours" type="text" class="form-control">
                   </div>
                   <div class="col-md-4">
                       <label>Price</label>
                       <input name="price" type="text" class="form-control">
                   </div>
              

                <div class="col-md-12 mt-2">
                    <label>Course Overview</label>
                    <textarea name="details" id="" cols="30" rows="10"></textarea>
                </div>
                <div class="col-md-12 mt-2">
                    <label>Course Outline</label>
                    <textarea name="course_outline" id="" cols="30" rows="10"></textarea>
                </div>
                
                <div class="col-md-12 mt-2">
                    <label>Venue</label>
                    <input name="venue" type="text" class="form-control">
                </div>
                <div class="col-md-12 mt-2">
                    <label>Who Can Join?</label>
                    <input name="join_requirement" type="text" class="form-control">
                </div>
                <div class="col-md-6 mb-2 mt-2">
                    <div class="form-group">
                        <label>Course Image:</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true">
                                </div>
                            </div>
                   
                </div>
                
                <div class="col-md-6 mb-2 mt-2">
                    <div class="form-group">
                        <label>Trainer Image:</label>
                                <div class="custom-file">
                                    <input type="file" name="trainer_image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true">
                                </div>
                            </div>
                  
                </div>
                   
                
                <div class="col-md-2 mt-2 text-center d-flex ml-auto">
                    <button type="submit" class="btn btn-primary"> Submit </button>
                </div>

            </div>
        </form>
    </div>
@endsection

