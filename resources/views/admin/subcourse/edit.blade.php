@extends('admin.layout.app')

@section('content')
    <h3 class="text-center mb-5 mt-5">Sub-Course Edit</h3>
    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
    <div class="col-md-12 ">
        <form action="{{route('sub-course.update',['id'=>$subcourse->id])}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-row">
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Course name" name="subcourse_name" value="{{$subcourse->subcourse_name}}">
                </div>
                <div class="col-md-6 mt-2">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1" name="course_id">
                            <option value="{{$subcourse->course_id}}">{{$subcourse->course->course_name}}</option>
                            @foreach($courses as $course)
                                <option value="{{$course->id}}">{{$course->course_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 mt-2">
                    <input name="trainer_name" type="text" placeholder="Trainer Name" class="form-control" value="{{$subcourse->trainer_name}}" required>
                </div>
                
                <div class="col-md-6">
                    <label class="mb-0 mt-1">Start Date:</label>
                    <input name="start date" type="date" class="form-control" value="{{$subcourse->start_date}}" required>
                </div>
                <div class="col-md-6">
                    <label class="mb-0 mt-1">End Date:</label>
                    <input name="end date" type="date" class="form-control" value="{{$subcourse->end_date}}" required>
                </div>
                
                    <div class="col-md-6 mb-2">
                       <label>Class Schedule:</label>
                       <input name="class_schedule" type="text" class="form-control" value="{{$subcourse->class_schedule}}">
                   </div>
                   <div class="col-md-6 mb-2">
                       <label>Tentative Class:</label>
                       <input name="tentative_class" type="date" class="form-control" value="{{$subcourse->tentative_class}}">
                   </div>
                   <div class="col-md-6 mb-2">
                       <label>Total Seat:</label>
                       <input name="total_seat" type="text" class="form-control" value="{{$subcourse->total_seat}}">
                   </div>
                   <div class="col-md-6 mb-2">
                       <label>Available Seat:</label>
                       <input name="available_seat" type="text" class="form-control" value="{{$subcourse->available_seat}}">
                   </div>
            
                   <div class="col-md-4">
                       <label>No. of Sesion</label>
                       <input name="session_no" type="text" class="form-control" value="{{$subcourse->session_no}}">
                   </div>
                   <div class="col-md-4">
                       <label>Total Hours</label>
                       <input name="hours" type="text" class="form-control" value="{{$subcourse->total_hours}}">
                   </div>
                   <div class="col-md-4">
                       <label>Price</label>
                       <input name="price" type="text" class="form-control" value="{{$subcourse->price}}">
                   </div>
              

                <div class="col-md-12 mt-2">
                    <label>Course Overview</label>
                    <textarea name="details" id="" cols="30" rows="10">{!! $subcourse->details !!}</textarea>
                </div>
                <div class="col-md-12 mt-2">
                    <label>Course Outline</label>
                    <textarea name="course_outline" id="" cols="30" rows="10">{!! $subcourse->course_outline !!}</textarea>
                </div>
                
                <div class="col-md-12 mt-2">
                    <label>Venue</label>
                    <input name="venue" type="text" class="form-control" value="{{$subcourse->venue}}">
                </div>
                <div class="col-md-12 mt-2">
                    <label>Who Can Join?</label>
                    <textarea name="join_requirement" class="form-control">{{$subcourse->join_requirement}}</textarea> 
                </div>
                <div class="col-md-6 mb-2 mt-2">
                    <div class="form-group">
                        <label>Course Image:</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true">
                                </div>
                            </div>
                    <div class="text-left mt-1">
                        <img src="{{asset('/image/subcourse/'.$subcourse->image)}}" style="height: 80px; width: 80px;">
                    </div>
                </div>
                
                <div class="col-md-6 mb-2 mt-2">
                    <div class="form-group">
                        <label>Trainer Image:</label>
                                <div class="custom-file">
                                    <input type="file" name="trainer_image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true">
                                </div>
                            </div>
                    <div class="text-left mt-1">
                        <img src="{{asset('/image/subcourse/'.$subcourse->trainer_image)}}" style="height: 80px; width: 80px;">
                    </div>
                </div>
                   
                
                <div class="col-md-2 mt-2 text-center d-flex ml-auto">
                    <button type="submit" class="btn btn-primary"> Submit </button>
                </div>

            </div>
        </form>
    </div>
@endsection

