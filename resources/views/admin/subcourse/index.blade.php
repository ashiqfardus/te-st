@extends('admin.layout.app')

@section('content')

    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6  class="float-left m-0 font-weight-bold text-primary">Sub-Courses</h6>
                <a class="float-right btn btn-primary btn-sm" href="{{route('sub-course.create')}}">Add New <i class="fa fa-plus"></i></a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Sub-Course name</th>
                            <th>Course name</th>
                            <th>Image</th>
                            <th>Course Overview</th>
                            <th>Active Status</th>
                            <th>On Going</th>
                            <th>Popular</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($subcourses as $post)
                            <tr>
                                <td>{{$n++}}</td>
                                <td>{{$post->subcourse_name}}</td>
                                <td>{{$post->course->course_name}}</td>
                                <td><img src="{{asset('/image/subcourse/'.$post->image)}}" style="height: 80px; width: 80px;"></td>
                                <!--<td> {!! str_limit($post->details, 100) !!} </td>-->
                                 <td> {!! $post->details !!} </td> 
                                <td>
                                    @if($post->active_status==0)
                                        <a href="{{route('sub-course.active_status',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Enable">Disabled</a>
                                       @else
                                        <a href="{{route('sub-course.active_status',['id'=>$post->id])}}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Disable">Enabled</a>
                                        @endif

                                </td>
                                 <td>
                                    @if($post->ongoing==0)
                                        <a href="{{route('sub-course.ongoing',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Enable">Disabled</i></a>
                                       @else
                                        <a href="{{route('sub-course.ongoing',['id'=>$post->id])}}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Disable">Enabled</a>
                                        @endif

                                </td>
                                <td>
                                    @if($post->popular==0)
                                        <a href="{{route('sub-course.popular',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Enable">Disabled</a>
                                       @else
                                        <a href="{{route('sub-course.popular',['id'=>$post->id])}}" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Disable">Enabled</i></a>
                                        @endif

                                </td>
                                 
                                <td>
                                    <a href="{{route('sub-course.edit',['id'=>$post->id])}}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="{{route('sub-course.delete',['id'=>$post->id])}}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection
