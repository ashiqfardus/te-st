@extends('layouts.main')

@section('content')
    <div class="pageBradcamb"  style="background-image:url({{asset('image/bradcamb.jpeg')}})">
        <div class="bradcambContent">
            <h2>Instructor Application</h2>
        </div>
    </div>

    <div class="contactMain" style="padding:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-2">
                    @if (session('alert'))
                        <div class="alert alert-success">
                            {{ session('alert') }}
                        </div>
                    @endif
                       @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="contact-form-area">
                        <form class="contact-from" action="{{route('teacher_form')}}" method="post" enctype="multipart/form-data">

                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="name" id="contact-name" placeholder="Full Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="contact-website" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="number" id="contact-email" placeholder="Contact Number" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="address" id="contact-email" placeholder="Address" required>
                            </div>

                            <div class="form-group">
                                <span style="color: black; ">CV (PDF,DOC):</span>
                                 <input type="file" name="cv" class="dropify" data-allowed-file-extensions="pdf doc docx" data-show-errors="true" data-show-loader="true" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" > Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
