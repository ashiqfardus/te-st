@extends('layouts.main')

@section('banner')
    <section class="banner-area relative" style="background-image: url({{asset('image/slider/'.$sliders)}});" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row fullscreen d-flex align-items-center justify-content-between">
                <div class="banner-content col-lg-8 offset-lg-2">
                    <h1 class="text-uppercase">
                        welcome to <span>IRT</span>
                    </h1>
                    <p>The purpose of education is to turn mirrors into windows.</p>
                    <a href="{{route('student_apply')}}" class="primary-btn text-uppercase">Get Started</a>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('feature')
    <section class="feature-area">
        <div class="container">
            <div class="row">
                
                @foreach($ongoing as $og)
                <div class="col-lg-3">
                    <div class="single-feature">
                        <!--<div class="featureIcon">-->
                        <!--    <i class="fa fa-book"></i>-->
                        <!--</div>-->
                        <div class="sfeatureContent">
                            <a href="{{route('course_details',['id'=>$og->id])}}" >{{$og->subcourse_name}}</a>
                            <p>(On Going)</p>
                        </div>
                    </div>
                </div>
                @endforeach
             
          
               
            </div>
        </div>
    </section>
    @endsection


@section('content')

    <section class="popular-course-area section-gap">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-50 col-lg-8 pt-15">
                    <div class="title text-center">
                        <h1>Popular Courses we offer</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-popular-carusel">

                    @foreach($subcourses as $sub)
                        @if($sub->popular==1)
                    <div class="single-popular-carusel">
                        <div class="thumb-wrap relative">
                            <div class="thumb relative">
                                <div class="overlay overlay-bg"></div>
                                <img class="img-fluid" src="{{asset('image/subcourse/'.$sub->image)}}" alt="">
                            </div>
                        </div>
                        <div class="details">
                            <a href=""><h4>{{$sub->subcourse_name}}</h4></a>
                            <p>{!! str_limit($sub->details,185)!!}</p>
                        </div>
                    </div>
                        @endif
                        @endforeach
                </div>
            </div>
        </div>
    </section>



{{--    Testimonial--}}
    <section class="review-area section-gap relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-5 col-lg-8 pt-1">
                    <div class="title text-center">
                        <h1 class="mb-10">What Students Says</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-review-carusel">

                    @foreach($testimonial as $t)
                    <div class="single-review item">
                        <div class="authorImg">
                            <img src="{{asset('image/testimonial/'.$t->image)}}" alt="">
                        </div>
                        <div class="title justify-content-start d-flex">
                            <a><h4>{{$t->name}}</h4></a>
                        </div>
                        <p>{!! $t->details !!}</p>
                    </div>
                        @endforeach
                </div>
            </div>
        </div>
    </section>


    <section class="cta-one-area relative section-gap">
        <div class="container">
            <div class="overlay overlay-bg"></div>
            <div class="row justify-content-center">
                <div class="wrap">
                    <h1 class="text-white">Become an instructor</h1>
                    <p>There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s exciting to think about setting up your own viewing station whether that is on the deck.</p>
                    <a class="primary-btn wh" href="{{route('teacher_apply')}}">Apply for the post</a>
                </div>
            </div>
        </div>
    </section>
    @endsection
