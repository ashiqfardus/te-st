<?php
$email_to = "ashiqfardus@hotmail.com";
$email_subject = "IRT Contact Message";

function died($error) {
    // your error code can go here


    echo "We are very sorry, but there were error(s) found with the form you submitted. ";
    echo "These errors appear below.<br /><br />";
    echo $error."<br /><br />";
    echo "Please go back and fix these errors.<br /><br />";
    die();
}


$name = $data->name; // required
$email_from = $data->email; // required
$subject = $data->subject; // required
$message = $data->message; // required

function clean_string($string) {
    $bad = array("content-type","bcc:","to:","cc:","href");
    return str_replace($bad,"",$string);
}

$email_message='';
$email_message .= "Name: ".clean_string($name)."\n";
$email_message .= "Email: ".clean_string($email_from)."\n";
$email_message .= "Subject: ".clean_string($subject)."\n";
$email_message .= "Message: ".clean_string($message)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
    'Reply-To: '.$email_from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
if (@mail($email_to, $email_subject, $email_message, $headers))
{
    $actual_link = $_SERVER['HTTP_HOST'];
    echo "<script>alert('You message has been sent. We will contact you soon.');
    </script>";
    header('Location:'.$actual_link);
}



