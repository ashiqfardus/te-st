

<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('image/play.png')}}">
    <meta name="author" content="techsolutions">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="UTF-8">
    <title>IRT</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.eot" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.svg" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.ttf" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/fonts/dropify.woff" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="{{asset('css/linearicons.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
</head>
<body>
<header id="header" id="home">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
                    <a href="tel:+953 012 3654 896"><span class="lnr lnr-phone-handset"></span> <span class="text">{{$contacts->phone}}</span></a>
                    <a href="mailto:support@campustocorporate.com"><span class="lnr lnr-envelope"></span> <span class="text">{{$contacts->email }}</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="{{route('index')}}"><img src="{{asset('image/logo/'.$logo->image)}}" alt="" title="" style="height: 100px; width: 187px;" /></a>
                <!--<h2>IRT <span>Institute of Research & Training</span></h2>-->

            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li><a href="{{route('index')}}">Home</a></li>
                    <li class="menu-has-children"><a href="#">Courses</a>
                        <!--<ul>-->
                        <!--	<li><a href="#">Programming PHP</a></li>		-->
                        <!--	<li><a href="#">Networking</a></li>		-->
                        <!--	<li><a href="#">Graphic Design</a></li>				                		-->
                        <!--	<li><a href="#">Modern Javascript</a></li>				                		-->
                        <!--</ul>-->

                        <div id='megaMenu'>

                            @foreach($course as $c)
                            <div class='singleMegaCourse' onclick="window.location.href='{{route('course',['id'=>$c->id])}}'">
                                <div class='singmegcIcon'>
                                    <img src='{{asset('image/course/'.$c->icon)}}'>
                                </div>
                                <div class='singmegcTitleInfo'>
                                    <h3>{{$c->course_name}}</h3>
{{--                                    <p>Digital Drawing & Illustration</p>--}}
                                </div>

                                <ol class='megaMenuSub'>
                                    @foreach($subcourses as $s)
                                        @if($s->course_id==$c->id)
                                    <li><a href="{{route('course_details',['id'=>$s->id])}}">{{$s->subcourse_name}} </a></li>
                                        @endif
                                        @endforeach
                                </ol>
                            </div>
                                @endforeach
                        </div>
                    </li>
                    <li><a href="{{route('about')}}">About</a></li>
                    <!--<li><a href="#">Events</a></li>-->
                    <!--<li><a href="#">Gallery</a></li>-->
                    <!--<li><a href="#">Blog</a></li>	-->

                    <li><a href="{{route('contact')}}">Contact</a></li>
                </ul>
            </nav><!-- #nav-menu-container -->
            <a href='{{route('student_apply')}}' id='applyNow'>Apply Now</a>
        </div>
    </div>
</header>
<!-- start banner Area -->
@yield('banner')
<!-- End banner Area -->
<!-- Start feature Area -->
@yield('feature')
<!--Main content-->
@yield('content')

<!-- MAin content end here-->

<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="single-footer-widget">
{{--                    <a href="{{route('index')}}"><img src="{{asset('image/logo/'.$logo->image)}}" alt="" style="width: 150px; height: auto;"></a>--}}
                    <h2 class="footerLogo">Institute of Research & Training</h2>
                    <p>One Of The Best It Training
                        Institute In Bangladesh.
                        We Make Professionals.</p>
                    <div class="footer-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="single-footer-widget">
                    <h4>Contact</h4>
                    <div class='footerContact'>
                        <div class='singleContact'>
                            <p><b>Location: </b>{{$contacts->address}}</p>
                            <p><b>Phone: </b>{{$contacts->phone}}</p>
                            <p><b>Email: </b>{{$contacts->email}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom row align-items-center justify-content-between">
            <p class="footer-text m-0 col-lg-6 col-md-12">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </a></p>

                <p class="mt-3">Design And Development by <a href="https://techsolutionsbd.com">Techsolutions</a></p>

        </div>
    </div>
</footer>
<!-- End footer Area -->




                


{{--<script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>--}}
<script
    src="https://code.jquery.com/jquery-1.12.4.min.js"
    integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<script>
    @if(Session::has('success'))
    toastr.success('{{Session::get('success')}}');
    @endif
    @if(Session::has('info'))
    toastr.info('{{Session::get('info')}}');
    @endif




    $(document).ready(function() {
       // Basic
       $('.dropify').dropify();
       // Translated
       $('.dropify-fr').dropify({
           messages: {
               default: 'Drag and drop a file here or click',
               replace: 'Drag and drop a file or click to replace',
               remove: 'Remove',
               error: 'Sorry, the file is too large'
           }
       });
       // Used events
      var drEvent = $('#input-file-events').dropify();

       drEvent.on('dropify.beforeClear', function(event, element) {
           return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
       });
       drEvent.on('dropify.afterClear', function(event, element) {
           alert('File deleted');
       });
       drEvent.on('dropify.errors', function(event, element) {
           console.log('Has Errors');
       });
drEvent.on('dropify.error.fileSize', function(event, element){
alert('Filesize error message!');
});
drEvent.on('dropify.error.minWidth', function(event, element){
alert('Min width error message!');
});
drEvent.on('dropify.error.maxWidth', function(event, element){
alert('Max width error message!');
});
drEvent.on('dropify.error.minHeight', function(event, element){
alert('Min height error message!');
});
drEvent.on('dropify.error.maxHeight', function(event, element){
alert('Max height error message!');
});
drEvent.on('dropify.error.imageFormat', function(event, element){
alert('Image format error message!');
});

       var drDestroy = $('#input-file-to-destroy').dropify();
       drDestroy = drDestroy.data('dropify')
       $('#toggleDropify').on('click', function(e) {
           e.preventDefault();
           if (drDestroy.isDropified()) {
               drDestroy.destroy();
           } else {
               drDestroy.init();
           }
       })
   });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.assets/js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{asset('js/easing.min.js')}}"></script>
<script src="{{asset('js/hoverIntent.js')}}"></script>
<script src="{{asset('js/superfish.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/jquery.tabs.min.js')}}"></script>
<script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/mail-script.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script>
<script>


    $('#course').on('change',function (e) {
        var course_id =e.target.value;
      
        $.get('student/fetch?course_id='+ course_id, function (data) {
            $('#subcourse').empty();
            $.each(data,function (student_apply, subcourseObj) {
                $('#subcourse').append('<option value="'+subcourseObj.id +'">'+subcourseObj.subcourse_name+'</option>');
            })


        });
    })
    $('#course_details').on('change',function (e) {
        var course_id =e.target.value;
      
        $.get('/course_details/fetch?course_id='+ course_id, function (data) {
            $('#subcourse_details').empty();
            $.each(data,function (student_apply, subcourseObj) {
                $('#subcourse_details').append('<option value="'+subcourseObj.id +'">'+subcourseObj.subcourse_name+'</option>');
            })


        });
    })
</script>
</body>
</html>
