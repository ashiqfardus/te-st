@extends('layouts.main')

@section('content')

    <div class="pageBradcamb"  style="background-image:url({{asset('image/bradcamb.jpeg')}})">
        <div class="bradcambContent">
            <h2>Courses</h2>
        </div>
    </div>
    <div class="coursesSection">
        <div class="container">
            <div class="row">
                @foreach($allcourse as $s)
                <div class="col-md-4">
                    <div class="course-single-item">
                        <div class="course-single-thumb">
                            <img src="{{asset('image/subcourse/'.$s->image)}}" alt="course-image" style="height:220px;">
                        </div>
                        <div class="course-single-content">
                            <h4 class="course-title"><a href="{{route('course_details',['id'=>$s->id])}}">{{$s->subcourse_name}}</a></h4>
                            <p>{!! str_limit($s->details,170) !!}...</p>
                            <div class="course-bottom">
                                <ul class="course-bottom-list">
                                    <li>
                                        <a href="{{route('student_apply')}}" class="btnEnroll">
                                            Enroll Now
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <div class="course-duration text-right">
                                          <a href="{{route('course_details',['id'=>$s->id])}}">Read More</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
    @endsection
