@extends('layouts.main')

@section('content')
    <div class="pageBradcamb"  style="background-image:url({{asset('image/bradcamb.jpeg')}})">
        <div class="bradcambContent">
            <h2>Student Application</h2>
        </div>
    </div>

    <div class="contactMain" style="padding:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-2">
                    @if (session('alert'))
                        <div class="alert alert-success">
                            {{ session('alert') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="contact-form-area">
                        <form class="contact-from" action="{{route('student_form')}}" method="post" enctype="multipart/form-data">

                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="name" id="contact-name" placeholder="Full Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="contact-website" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="number" name="number" id="contact-email" placeholder="Contact Number" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="address" id="contact-email" placeholder="Address" required>
                            </div>
                            <div class="form-group">
                                <select class="form-control dynamic" name="course" id="course" required>
                                    <option value="">Choose a course</option>

                                    @foreach($course as $c)
                                    <option value="{{$c->id}}">{{$c->course_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="subcourse" id="subcourse" required>
                                    
                                </select>
                            </div>

                            <div class="form-group">
                                <span style="color: black; ">Birthdate:</span>
                                <input type="date" name="birthdate" required>
                            </div>
                            <div class="form-group">
                                <span style="color: black; ">Student Image:</span>
                                <div class="custom-file">
                                    <input type="file" name="image" class="dropify" data-allowed-file-extensions="jpg png jpeg" data-show-errors="true" data-show-loader="true" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="text" name="qualification" id="contact-email" placeholder="Last Education Qualification" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="institute" id="contact-email" placeholder="Institute Name" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" > Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @endsection
