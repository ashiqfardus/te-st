@extends('layouts.main')

@section('content')
    <div class="pageBradcamb"  style="background-image:url({{asset('image/bradcamb.jpeg')}})">
        <div class="bradcambContent">
            <h2>Contact</h2>
        </div>
    </div>
    <div class="contactMain" style="padding:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="contact-form-area">
                        <h2 class="contact-form-title">We Love to Hear From You</h2>
                        @if (session('alert'))
                            <div class="alert alert-success">
                                {{ session('alert') }}
                            </div>
                        @endif
                        <form class="contact-from" action="{{route('contact.mail')}}" method="post">

                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="name" id="contact-name" placeholder="Full Name" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="contact-email" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="subject" id="contact-email" placeholder="Subject" required>
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" placeholder="Comments" required></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary ">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-address-block">
                        <span>Our Offices</span>
                        <h4 class="contact-address-title">Contact Info</h4>
                        <ul class="address-items-list">
                            <li class="address-single-item">
                                <div class="address-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="address-content">
                                    <h5>Find Us</h5>
                                    <p>{{$contacts->address}}</p>
                                </div>
                            </li><!-- address-single-item end -->
                            <li class="address-single-item">
                                <div class="address-icon">
                                    <i class="fa fa-phone"></i>

                                </div>
                                <div class="address-content">
                                    <h5>Call Us</h5>
                                    <p>{{$contacts->mobile}}</p>
                                </div>
                            </li><!-- address-single-item end -->
                            <li class="address-single-item">
                                <div class="address-icon">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="address-content">
                                    <h5>Mail Us</h5>
                                    <p>{{$contacts->email}}</p>
                                </div>
                            </li><!-- address-single-item end -->
                        </ul>
                    </div>
                </div>
            </div>
            </div>
            </div>
    @endsection
