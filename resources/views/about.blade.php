@extends('layouts.main')

@section('content')
    <div class="pageBradcamb"  style="background-image:url({{asset('image/bradcamb.jpeg')}})">
        <div class="bradcambContent">
            <h2>About Us</h2>
        </div>
    </div>

    <div class="aboutUsPageMain">
        <div class="container">
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="whoWeAreContent">
                        {!! $about->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
